"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CrudActions = /** @class */ (function () {
    function CrudActions() {
    }
    /** create
     *
     * @description Posts a new item to the API and updates the state with the item
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param payload The new object to create and post
     * @returns Action of type CREATE that will be handled by the reducer
     */
    CrudActions.prototype.create = function (type, payload) {
        return { type: 'CREATE_' + type, payload: payload };
    };
    /** delete
     *
     * @description Deletes an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to delete
     * @returns Action of type DELETE that will be handled by the reducer
     */
    CrudActions.prototype.delete = function (type, id) {
        return { type: 'DELETE_' + type, id: id };
    };
    /** get
     *
     * @description Gets an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to get
     * @returns Action of type GET that will be handled by the reducer
     */
    CrudActions.prototype.get = function (type, id) {
        return { type: 'GET_' + type, id: id };
    };
    /** getMany
     *
     * @description Gets all the items from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type GET_MANY that will be handled by the reducer
     */
    CrudActions.prototype.getMany = function (type) {
        return { type: 'GET_MANY_' + type };
    };
    /** update
     *
     * @description Updates an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to update
     * @param updatedPayload The updated object
     * @returns Action of type UPDATE that will be handled by the reducer
     */
    CrudActions.prototype.update = function (type, id, updatedPayload) {
        return { type: 'UPDATE_' + type, id: id, payload: updatedPayload };
    };
    /** setCreating
     *
     * @description Sets the state to be creating
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type SET_CREATING that will be handled by the reducer
     */
    CrudActions.prototype.setCreating = function (type) {
        return { type: 'SET_CREATING_' + type };
    };
    /** setUpdating
     *
     * @description Sets the state to be updating
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type SET_UPDATING that will be handled by the reducer
     */
    CrudActions.prototype.setUpdating = function (type) {
        return { type: 'SET_UPDATING_' + type };
    };
    CrudActions = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CrudActions);
    return CrudActions;
}());
exports.CrudActions = CrudActions;
