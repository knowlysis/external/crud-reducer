import { tassign } from 'tassign';


export const crudReducer = (name = '', reducer) => (state, action) => {
    switch (action.type) {
        case 'CREATE_' + name:
            action.name = name;
            return {...state, isLoading: true};
        case 'CREATED_' + name:
            return {
                ...state,
                list: [...state.list, action.payload],
                isLoading: false,
                errorDetails: '',
            };
        case 'DELETE_' + name:
            action.name = name;
            return {...state, isLoading: true};
        case 'DELETED_' + name:
            return tassign({}, state, {
                ...state,
                isLoading: false
            });
        case 'GET_' + name:
            action.name = name;
            return {...state, isLoading: true};
        case 'GOT_' + name:
            return {...state, isLoading: false, errorDetails: '', selectedItem: action.payload};
        case 'GET_MANY_' + name:
            action.name = name;
            return {...state, isLoading: true};
        case 'GOT_MANY_' + name:
            return tassign({}, state, {list: action.payload, isLoading: false, errorDetails: ''});
        case 'UPDATE_' + name:
            action.name = name;
            return {...state, isLoading: true, selectedItem: action.payload};
        case 'UPDATED_' + name:
            return updateItem(state, action);
        case name + '_ACTION_FAILED':
            return {...state, errorDetails: action.payload};
        case 'SET_CREATING_' + name:
            return {...state, selectedItem: null, isCreating: true};
        case 'SET_UPDATING_' + name:
            return {...state, isCreating: false};
        case 'SET_EDITING_' + name:
            return {...state, isEditing: action.payload};
        case 'SET_SELECTED_' + name:
            return {...state, selectedItem: action.payload};
        default:
            return reducer(state, action);
    }
};
    
function updateItem(state, action) {
    const updatedList:any = [];
    
/*
        THIS NEEDS TO WORK GENERICALLY WITHOUT USING ID
*/

    state.list.forEach((item:any) => {
        updatedList.push(tassign({}, item));
    });
    updatedList[updatedList.map(item => item.id).indexOf(action.id)] = action.payload;
    
    return {
        ...state,
        list: updatedList,
        isLoading: false,
        errorDetails: ''
    };
}




