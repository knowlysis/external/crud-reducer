import { Injectable } from '@angular/core';
import { EnhancedAction } from './helpers';

@Injectable({
  providedIn: 'root'
})
export class CrudActions {
  /** create
   *
   * @description Posts a new item to the API and updates the state with the item
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @param payload The new object to create and post
   * @returns Action of type CREATE that will be handled by the reducer
   */
  create(type: string, payload: any, widgetId?: string, tab?: string): EnhancedAction {
    return { type: 'CREATE_' + type, payload, widgetId, tab };
  }

  /** delete
   *
   * @description Deletes an item from the API and updates the state
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @param id The id of the item to delete
   * @returns Action of type DELETE that will be handled by the reducer
   */
  delete(type: string, id: any, widgetId?: string): EnhancedAction {
    return { type: 'DELETE_' + type, id, widgetId };
  }

  /** get
   *
   * @description Gets an item from the API and updates the state
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @param id The id of the item to get
   * @returns Action of type GET that will be handled by the reducer
   */
  get(type: string, id: any, widgetId?: string, tab?: string): EnhancedAction {
    return { type: 'GET_' + type, id, widgetId, tab };
  }

  /** getMany
   *
   * @description Gets all the items from the API and updates the state
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @returns Action of type GET_MANY that will be handled by the reducer
   */
  getMany(type: string, id?: any, widgetId?: string): EnhancedAction {
    return { type: 'GET_MANY_' + type, id, widgetId };
  }

  /** update
   *
   * @description Updates an item from the API and updates the state
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @param id The id of the item to update
   * @param updatedPayload The updated object
   * @returns Action of type UPDATE that will be handled by the reducer
   */
  update(type: string, id: any, updatedPayload: any, widgetId?: string, tab?: string): EnhancedAction {
    return { type: 'UPDATE_' + type, id, payload: updatedPayload, widgetId, tab };
  }

  /** setCreating
   *
   * @description Sets the state to be creating
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @returns Action of type SET_CREATING that will be handled by the reducer
   */
  setCreating(type: string, widgetId?: string): EnhancedAction {
    return { type: 'SET_CREATING_' + type, widgetId };

  }

  /** setUpdating
   *
   * @description Sets the state to be updating
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @returns Action of type SET_UPDATING that will be handled by the reducer
   */
  setUpdating(type: string, widgetId?: string): EnhancedAction {
    return { type: 'SET_UPDATING_' + type, widgetId };
  }

    /** setEditing
   *
   * @description Sets the state to be editing
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @returns Action of type SET_EDITING that will be handled by the reducer
   */
  setEditing(type: string, payload, widgetId?: string): EnhancedAction {
    return { type: 'SET_EDITING_' + type, payload, widgetId };
  }

    /** setSelectedItem
   *
   * @description Sets the selected item of the state
   * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
   * @returns Action of type SET_SELECTED_ that will be handled by the reducer
   */
  setSelectedItem(type: string, payload: any, widgetId?: string) {
      return { type: 'SET_SELECTED_' + type, payload, widgetId };
  }
}
