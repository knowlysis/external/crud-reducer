"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var redux_observable_1 = require("redux-observable");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var CrudService = /** @class */ (function () {
    /** CrudService constructor */
    function CrudService(http) {
        var _this = this;
        this.http = http;
        /** Categories of actions to be appended to the generic action name */
        this.actions = [
            'COUNTRY',
            'AIRPORT'
        ];
        /** Used to link the @var action categories with an API route */
        this.routes = {
            'COUNTRY': '/country',
            'AIRPORT': '/airport'
        };
        this.apiRoute = 'api/';
        /** create
         *
         * @description Listens for CREATE action and posts payload to the API.
         * @returns Action of type CREATED on success and action of type ACTION_FAILED
         * upon failure.
         */
        this.create = function (action$) {
            return action$.pipe(redux_observable_1.ofType.apply(void 0, _this.combineArray('CREATE_', _this.actions.slice())), operators_1.mergeMap(function (action) {
                // return this.http.post(this.apiRoute + this.routes[action.name], {})
                //         .pipe(
                //           map(result => {
                //             return {type: 'CREATED_' + action.name, payload: action.payload};
                //           }),
                //           catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
                //         );
                return Promise.resolve({ type: 'CREATED_' + action.name, payload: action.payload });
            }));
        };
        /** delete
         *
         * @description Listens for DELETE action and deletes item with id in the API.
         * @returns Action of type DELETED on success and action of type ACTION_FAILED
         * upon failure.
         */
        this.delete = function (action$) {
            return action$.pipe(redux_observable_1.ofType.apply(void 0, _this.combineArray('DELETE_', _this.actions.slice())), operators_1.mergeMap(function (action) {
                return Promise.resolve({ type: 'DELETED_' + action.name, id: action.id });
            }));
        };
        /** get
         *
         * @description Listens for GET action and gets item with id in the API.
         * @returns Action of type GOT on success and action of type ACTION_FAILED
         * upon failure.
         */
        this.get = function (action$) {
            return action$.pipe(redux_observable_1.ofType.apply(void 0, _this.combineArray('GET_', _this.actions.slice())), operators_1.mergeMap(function (action) {
                return Promise.resolve({ type: 'GOT_' + action.name, id: action.id, payload: null });
            }));
        };
        /** getMany
         *
         * @description Listens for GET_MANY action and gets all items in the API.
         * @returns Action of type CREATED on success and action of type ACTION_FAILED
         * upon failure.
         */
        this.getMany = function (action$) {
            return action$.pipe(redux_observable_1.ofType.apply(void 0, _this.combineArray('GET_MANY_', _this.actions.slice())), operators_1.mergeMap(function (action) {
                return _this.http.get('https://restcountries.eu/rest/v2/all')
                    .pipe(operators_1.map(function (result) {
                    return { type: 'GOT_MANY_' + action.name, payload: result };
                }), operators_1.catchError(function (error) { return rxjs_1.of({ type: action.name + '_ACTION_FAILED', payload: error }); }));
            }));
        };
        /** update
         *
         * @description Listens for UPDATE action and updates item with @var action.id in the API.
         * @returns Action of type CREATED on success and action of type ACTION_FAILED
         * upon failure.
         */
        this.update = function (action$) {
            return action$.pipe(redux_observable_1.ofType.apply(void 0, _this.combineArray('UPDATE_', _this.actions.slice())), operators_1.mergeMap(function (action) {
                return Promise.resolve({ type: 'UPDATED_' + action.name, id: action.id, payload: action.payload });
            }));
        };
        /** Combines the individual epics to be used elsewhere */
        this.crudEpics = redux_observable_1.combineEpics(this.create, this.delete, this.get, this.getMany, this.update);
    }
    /** Combines actions with the full name */
    CrudService.prototype.combineArray = function (type, actions) {
        for (var i = 0; i < actions.length; i++) {
            actions[i] = type + actions[i];
        }
        return actions;
    };
    CrudService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CrudService);
    return CrudService;
}());
exports.CrudService = CrudService;
