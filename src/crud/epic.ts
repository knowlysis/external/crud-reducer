import { Injectable, Inject, Optional, forwardRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { combineEpics, ofType } from 'redux-observable';
import { of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { EnhancedAction } from './helpers';


@Injectable({
  providedIn: 'root'
})

export class CrudService {
  actions: Array<string>;
  apiRoute: string;
  routes: {};

  /** CrudService constructor */
  constructor(@Inject(forwardRef(() => HttpClient)) private http: HttpClient) { }

  /** provideValues 
   * @description Provides the values needed to resolve API calls.
   * @param actions The array of action names
   * @param apiRoute The base route of the API
   * @param routes An object with the action names as keys and the associated route as the value
   */
  provideValues(actions: Array<string>, apiRoute: string, routes: any) {
    this.actions = actions;
    this.apiRoute = apiRoute;
    this.routes = routes;
  }

  /** Combines actions with the full name */
  combineArray(type: string, actions: string[]) {
    for (let i = 0; i < actions.length; i++) {
      actions[i] = type + actions[i];
    }
    return actions;
  }

  /** create
   *
   * @description Listens for CREATE action and posts payload to the API.
   * @returns Action of type CREATED on success and action of type ACTION_FAILED
   * upon failure.
   */
  create = action$ =>
    action$.pipe(
      ofType(...this.combineArray('CREATE_', [...this.actions])),
      mergeMap((action: EnhancedAction): any => {
        return this.http.post(this.apiRoute + this.routes[action.name], action.payload)
                .pipe(
                    map(result => {
                        return {type: 'CREATED_' + action.name, payload: result, widgetId: action.widgetId, tab: action.tab};
                    }),
                    catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
                );
      })
    )

  /** delete
   *
   * @description Listens for DELETE action and deletes item with id in the API.
   * @returns Action of type DELETED on success and action of type ACTION_FAILED
   * upon failure.
   */
  delete = action$ =>
    action$.pipe(
      ofType(...this.combineArray('DELETE_', [...this.actions])),
      mergeMap((action: EnhancedAction): any => {
        return this.http.delete(this.apiRoute + this.routes[action.name] + '/' + action.id, {responseType: 'text'})
                .pipe(
                    map(result => {
                        return {type: 'DELETED_' + action.name, payload: action.id, widgetId: action.widgetId};
                    }),
                    catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
                );
      })
    )

  /** get
   *
   * @description Listens for GET action and gets item with id in the API.
   * @returns Action of type GOT on success and action of type ACTION_FAILED
   * upon failure.
   */
  get = action$ =>
    action$.pipe(
      ofType(...this.combineArray('GET_', [...this.actions])),
      mergeMap((action: EnhancedAction): any => {
        return this.http.get(this.apiRoute + this.routes[action.name] + '/' + action.id)
            .pipe(
                map(result => {
                    return {type: 'GOT_' + action.name, payload: result, widgetId: action.widgetId, tab: action.tab};
                }),
                catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
            );
      })
    )

  /** getMany
   *
   * @description Listens for GET_MANY action and gets all items in the API.
   * @returns Action of type CREATED on success and action of type ACTION_FAILED
   * upon failure.
   */
  getMany = action$ =>
    action$.pipe(
      ofType(...this.combineArray('GET_MANY_', [...this.actions])),
      mergeMap((action: EnhancedAction): any => {
        return this.http.get(this.apiRoute + this.routes[action.name] + '/all' + (action.id ? '/' + action.id : ''))
            .pipe(
                map(result => {
                    return {type: 'GOT_MANY_' + action.name, payload: result, widgetId: action.widgetId};
                }),
                catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
            );
      })
    )

  /** update
   *
   * @description Listens for UPDATE action and updates item with @var action.id in the API.
   * @returns Action of type CREATED on success and action of type ACTION_FAILED
   * upon failure.
   */
  update = action$ =>
    action$.pipe(
      ofType(...this.combineArray('UPDATE_', [...this.actions])),
      mergeMap((action: EnhancedAction): any => {
        return this.http.put(this.apiRoute + this.routes[action.name] + '/' + action.id, action.payload)
            .pipe(
                map(result => {
                    return {type: 'UPDATED_' + action.name, payload: result, widgetId: action.widgetId, tab: action.tab};
                }),
                catchError(error => of({type: action.name + '_ACTION_FAILED', payload: error}))
            );
      })
    )

  /** Combines the individual epics to be used elsewhere */
  crudEpics = combineEpics(this.create,
                            this.delete,
                            this.get,
                            this.getMany,
                            this.update);
}
