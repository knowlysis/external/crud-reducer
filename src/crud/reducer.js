"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var tassign_1 = require("tassign");
exports.crudReducer = function (name, reducer) {
    if (name === void 0) { name = ''; }
    return function (state, action) {
        switch (action.type) {
            case 'CREATE_' + name:
                action.name = name;
                return __assign({}, state, { isLoading: true });
            case 'CREATED_' + name:
                return __assign({}, state, { list: state.list.concat([action.payload]), isLoading: false, errorDetails: '' });
            case 'DELETE_' + name:
                action.name = name;
                return state;
            case 'DELETED_' + name:
                return tassign_1.tassign({}, state, __assign({ list: state.list.splice(state.list.map(function (item) { return item.id; }).indexOf(action.id), 1) }, state));
            case 'GET_' + name:
                action.name = name;
                return __assign({}, state, { isLoading: true });
            case 'GOT_' + name:
                return __assign({}, state, { isLoading: false, errorDetails: '', selectedItem: state.list[state.list.map(function (item) { return item.id; }).indexOf(action.id)] });
            case 'GET_MANY_' + name:
                action.name = name;
                return __assign({}, state, { isLoading: true });
            case 'GOT_MANY_' + name:
                return tassign_1.tassign({}, state, { list: action.payload, isLoading: false, errorDetails: '' });
            case 'UPDATE_' + name:
                action.name = name;
                return __assign({}, state, { isLoading: true });
            case 'UPDATED_' + name:
                return updateItem(state, action);
            case name + '_ACTION_FAILED':
                return __assign({}, state, { errorDetails: action.payload });
            case 'SET_CREATING_' + name:
                return __assign({}, state, { selectedItem: null, isCreating: true });
            case 'SET_UPDATING_' + name:
                return __assign({}, state, { isCreating: false });
            default:
                return reducer(state, action);
        }
    };
};
function updateItem(state, action) {
    var updatedList = [];
    state.list.forEach(function (item) {
        updatedList.push(tassign_1.tassign({}, item));
    });
    updatedList[updatedList.map(function (item) { return item.id; }).indexOf(action.id)] = action.payload;
    return {
        list: updatedList,
        isLoading: false,
        errorDetails: ''
    };
}
