import { Action } from 'redux';
export interface EnhancedAction extends Action {
    type: string;
    payload?: any;
    id?: any;
    name?: string;
}
