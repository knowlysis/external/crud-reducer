import { Action } from 'redux';

export interface EnhancedAction extends Action {
  type: string;
  payload?: any;
  id?: any;
  name?: string;
  widgetId?: string;
  tab?: string;
}

export interface CrudState {
    list: Array<any>;
    selectedItem: any;
    isLoading: boolean;
    isCreating: boolean;
    isEditing: boolean;
    errorDetails: string;
  }
