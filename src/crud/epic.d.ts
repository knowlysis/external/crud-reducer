import { HttpClient } from '@angular/common/http';
export declare class CrudService {
    private http;
    /** Categories of actions to be appended to the generic action name */
    actions: string[];
    /** Used to link the @var action categories with an API route */
    routes: {
        'COUNTRY': string;
        'AIRPORT': string;
    };
    apiRoute: string;
    /** CrudService constructor */
    constructor(http: HttpClient);
    /** Combines actions with the full name */
    combineArray(type: string, actions: string[]): string[];
    /** create
     *
     * @description Listens for CREATE action and posts payload to the API.
     * @returns Action of type CREATED on success and action of type ACTION_FAILED
     * upon failure.
     */
    create: (action$: any) => any;
    /** delete
     *
     * @description Listens for DELETE action and deletes item with id in the API.
     * @returns Action of type DELETED on success and action of type ACTION_FAILED
     * upon failure.
     */
    delete: (action$: any) => any;
    /** get
     *
     * @description Listens for GET action and gets item with id in the API.
     * @returns Action of type GOT on success and action of type ACTION_FAILED
     * upon failure.
     */
    get: (action$: any) => any;
    /** getMany
     *
     * @description Listens for GET_MANY action and gets all items in the API.
     * @returns Action of type CREATED on success and action of type ACTION_FAILED
     * upon failure.
     */
    getMany: (action$: any) => any;
    /** update
     *
     * @description Listens for UPDATE action and updates item with @var action.id in the API.
     * @returns Action of type CREATED on success and action of type ACTION_FAILED
     * upon failure.
     */
    update: (action$: any) => any;
    /** Combines the individual epics to be used elsewhere */
    crudEpics: (action$: any) => any;
}
