import { EnhancedAction } from './helpers';
export declare class CrudActions {
    /** create
     *
     * @description Posts a new item to the API and updates the state with the item
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param payload The new object to create and post
     * @returns Action of type CREATE that will be handled by the reducer
     */
    create(type: string, payload: any): EnhancedAction;
    /** delete
     *
     * @description Deletes an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to delete
     * @returns Action of type DELETE that will be handled by the reducer
     */
    delete(type: string, id: number): EnhancedAction;
    /** get
     *
     * @description Gets an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to get
     * @returns Action of type GET that will be handled by the reducer
     */
    get(type: string, id: number): EnhancedAction;
    /** getMany
     *
     * @description Gets all the items from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type GET_MANY that will be handled by the reducer
     */
    getMany(type: string): EnhancedAction;
    /** update
     *
     * @description Updates an item from the API and updates the state
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param id The id of the item to update
     * @param updatedPayload The updated object
     * @returns Action of type UPDATE that will be handled by the reducer
     */
    update(type: string, id: number, updatedPayload: any): EnhancedAction;
    /** setCreating
     *
     * @description Sets the state to be creating
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type SET_CREATING that will be handled by the reducer
     */
    setCreating(type: string): EnhancedAction;
    /** setUpdating
     *
     * @description Sets the state to be updating
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @returns Action of type SET_UPDATING that will be handled by the reducer
     */
    setUpdating(type: string): EnhancedAction;
    /** setEditing
     *
     * @description Sets the state to be editing
     * @param type Name of the type of action ('COUNTRY', 'AIRPORT', etc.)
     * @param isEditing Whether the current item is editing or not
     * @returns Action of type SET_EDITING that will be handled by the reducer
     */
    setEditing(type: string, isEditing: boolean): EnhancedAction;
}
