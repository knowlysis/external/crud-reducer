# Crud Reducer

## Installation and Usage
To add this package to your application run:
```bash
npm i @microexcel-csd/crud-reducer
```

To use the crud reducer import it into any file with:
```js
import { crudReducer } from '@microexcel-csd/crud-reducer';
```

Then to implement the crud reducer, combine it with your own reducer:
```js
function siblingReducer(state = INITIAL_REDUCER_STATE, action) {
  switch (action.type) {
    case 'TEST_ACTION':
      return {};
    default: return state;
  }
}

export const myReducer = crudReducer('REDUCER', reducer);
```

Write your own custom reducer to replace siblingReducer in this example. It will be appended to the end of the crud reducer. Replace 'REDUCER' with a string identifying the type of object that will have crud actions performed on it. i.e. 'COUNTRY', 'STATE', or 'CITY'

## Crud Epic
To use crud epics to make http requests import the service into your module:
```js
import { CrudService } from '@microexcel-csd/crud-reducer';
```

Then add the CrudService to the providers array: 
```js
providers: [
  CrudService 
]
```

Then, we must supply 3 items to the CrudService: the actions, apiRoute, and routes. We do this in the same location where we provide the CrudService by calling the `provideValues` function:
```javascript
this.crudService.provideValues(
  [
    'ACTION_NAME',
    'OTHER_ACTION'
  ],
  'your-api-route',
  {
    'ACTION_NAME': '/action-route',
    'OTHER_ACTION': '/other-action-route'
  }
);
```

Your **apiRoute** should be a reference to the location of your route i.e. https://restcountries.eu/rest/v2/
The routes provide an object containing all the sub-routes to append to your apiRoute.
Side note: the GET_MANY_ will append '/all' to the end of the request, so make sure the API is written accordingly.

The **actions** provide an array of action types that will be appended to the redux action, such as 'GET_COUNTRY'.

The **routes** item is simply an object with the action name as the key with the route for that action as the value.

The crud reducer requires a state object of the following shape: 
```javascript
export interface CrudState {
  list: Array<T>;
  selectedItem: T;
  isLoading: boolean;
  isCreating: boolean;
  errorDetails: string;
}
```

The Crud State can be imported from the project as well and has the above shape so you can create your own state as follows: 

```javascript
interface MyState extends CrudState {
    /* The CrudState attributes will be included here */
    myAttributes: string;
}
```

## Actions
The actions that the reducer handles are the following: 

*  'CREATE_' : Executes a post with the action.payload
*  'CREATED_' : Creates a new state object with created object appended to list
*  'DELETE_' : Executes a delete with the action.id
*  'DELETED_' : Creates a new state object without action.id in the list
*  'GET_' : Executes a get with the action.id
*  'GOT_' : Creates a new state object with selectedItem set to returned item
*  'GET_MANY_' : Executes a get with action.name
*  'GOT_MANY_' : Creates a new state object with list set to returned item
*  'UPDATE_' : Executes a put with the action.payload
*  'UPDATED_' : Creates a new state object with list updated with new item
*  'SET_CREATING_' : Tracks when the user is creating a new object
*  'SET_UPDATING_' : Tracks when the user is updating an existing object
*  '_ACTION_FAILED' : Contains the action.name and errorDetails from failed HTTP request


#### Support Our Packages
[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LM74WLHTJN8BA)

